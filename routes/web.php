<?php
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//pour changer la langue
App::setlocale('fr');

//route qui retourne vers l'index du site
Route::get('/', function () {
    return view('index');
});

 
//retoune la page pour les outils
Route::get('/outils', function () {
    return view('outils');
});

//retourne la page contact
Route::get('/contact', function () {
    return view('contact');
});

Route::get('/liste',function(){
    return view('ListEtudiant');
});

//pour faire la recherche d'un tuteur
Route::any('/searchTuto',function(){
    $q = Input::get ('rechercheTuteur');
    $user = User::where('prenom','LIKE','%'.$q.'%')->orWhere('name','LIKE','%'.$q.'%')->get();
    if(count($user) > 0)
        return view('tuto')->withDetails($user)->withQuery ( $q );
    else return view ('tuto')->withMessage('No Details found. Try to search again !');
});

//routes pour enregistrer un nouvel utilisateur
Route::get('/register', 'RegisterController@create')->name('register');
Route::post('register', 'RegisterController@insert');

//authentification pour le login
Route::get('/login','Auth\LoginController@getLogin')->name('login');
Route::post('login', 'Auth\LoginController@postLogin');
Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');

//afficher la page pour le profil de l'etudiant ou du professeur
//Route::resource('profil/{users}', 'ProfilController');
//Route::get('profil/{users}','ProfilController@index');
Route::get('profil/{users}','ProfilController@index')->name('profil');

//update si l'étudiant devient un tuteur
//Route::get('profil/{user_id}','ProfilController@updateEstTuteur');
Route::put('profil/{user_id}','ProfilController@updateEstTuteur')->name('updateTuteur');
Route::delete('profil/{user_id}','UserController@destroy')->name('delete');

//route qui retourne vers la vue pour voir la liste des tuteurs
Route::get('/tuteurs', 'TuteurController@index');
//afficher la page pour voir le profil du tuteur
Route::get('tuteurs/{users}', 'TuteurController@show');

//pour afficher les professeurs
Route::get('/professeurs','ProfesseursController@index');
//Route::resource('professeurs','ProfesseursController');
//Route::post('/professeurs/{id}','ProfesseursController@show');
Route::get('/professeurs/{user_id}','ProfesseursController@show');

//routes pour update un utilisateur
Route::put('/ModifierEtudiant/{id}', 'EtudiantsController@update')->name('ModifierEtudiant');
Route::put('/ModifierProf/{id}', 'ProfesseursController@update')->name('ModifierProf');

//reservation
Route::get('/reservation/tuteur/{id}','ReservationController@reserverTuteur')->name('debutTuteur');
Route::get('/reservation/professeur/{id}','ReservationController@reserverProfeseur')->name('debutProf');
Route::post('reservation/confirmation','ReservationController@store')->name('addReservation');
Route::get('evenements/{id}','ReservationController@getEvents')->name('evenements');





