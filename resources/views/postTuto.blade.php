@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
<div class="col">
    <img class="rounded responsive w-75 p-3" src="{{ asset('images/profile-icon.png')}}">   
</div>

<div class="col">
<form>
    <?php
        $tuto = DB::table('etudiants')->where('etudiant_id',$etudiant_tuteur->etudiant_id)->get()[0]
    ?>
<div class="row">
    <div class="col">
        <label>Nom tuteur:</label>
        <input type="text" value="{{$tuto->etudiant_nom}}" class="form-control" readonly/>
    </div>
    <div class="col">
        <label>Prénom:</label>
        <input type="text" value="{{$tuto->etudiant_prenom}}" class="form-control" readonly/>
    </div>  
</div>

    <div class="form-group">
        <label>Programme:</label>
        <input type="text" value="{{$tuto->etudiant_programme}}" class="form-control" readonly/>
    </div>

    <div class="form-group">
        <label>Campus:</label>
        <input type="text" value="{{$tuto->etudiant_campus}}" class="form-control" readonly/>
    </div>
    <div class="form-group">
        <label>À propos de moi:</label>
        <input  type="text" value="{{$tuto->etudiant_bio}}" class="form-control" readonly/>
    </div>
    
    <div class="form-group">
        <label>Cours offerts:</label>
        <input type="text" value="{{$etudiant_tuteur->tuteur_cours}}" class="form-control" readonly/>
    </div>

    <div class="form-group">
        <label>Disponibilités:</label>
        <input type="text" value="{{$etudiant_tuteur->disponibiliteTuteur}}" class="form-control" readonly/>
    </div>
    
</form>
    <button class="btn"><a href="/tuteurs">Retour</a></button>
    @if(Auth::check())
        <a href="{!! route('debutTuteur', ['etudiant_id'=>$tuto->etudiant_id]) !!}" class="btn btn-dark">Demander de l'aide (réserver)</a> 
    @else
        <p style="color:red">Vous devez etre connecte pour reserver du temps avec cette personne.</p>
    @endif
</div>

</div>
</div>
<hr>
@endsection