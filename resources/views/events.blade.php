@extends('layouts.app')

@section('content')
<div class="text-center">
    <h3>Évènement à venir</h3>
    <?php
        $reservations = DB::table('reservation')->where('etudiant_id', Auth::User()->user_id)->get();
        $tuto = DB::table('tuteur')->where('user_id', Auth::User()->user_id)->get();
        $professeur = DB::table('professeurs')->where('user_id', Auth::User()->user_id)->get();
        $reservation_personelle = [];
        
        foreach ($reservations as $key) {
            array_push($reservation_personelle, $key);
        }

        if(count($tuto) == 1)
        {
            $esttuto = DB::table('reservation')->where('tuteur_id', $tuto[0]->id)->get();

            foreach ($esttuto as $key) {
                array_push($reservation_personelle, $key);
            }
            
        }

    ?>
</div>
<table class="table">
    <thead>
        <tr>
            <th>Date</th>
            <th>Nom du tuteur</th>
            <th>Nom de l'etudiant</th>
            <th>Adresse courriel de l'etudiant</th>
            <th>Adresse courriel du tuteur</th>
            <th></th>
        </tr>
    </thead>
        @foreach ($reservations as $reservation)
        <tbody>
            <td>
                {{$reservation->reservation_date}}
            </td>
            <td>
                @if($reservation->tuteur_id != null)
                
                    {{(DB::table('etudiants')->where('user_id',(DB::table('tuteur')->where('id',$reservation->tuteur_id)->get())[0]->user_id)->get())[0]->etudiant_nom}} {{(DB::table('etudiants')->where('user_id',(DB::table('tuteur')->where('id',$reservation->tuteur_id)->get())[0]->user_id)->get())[0]->etudiant_prenom}} 
                
                @endif
                @if($reservation->professeur_id != null)
                    {{DB::table('professeurs')->where('professeur_id',$reservation->professeur_id)->get()[0]->professeur_prenom}}  {{DB::table('professeurs')->where('professeur_id',$reservation->professeur_id)->get()[0]->professeur_nom}}           
                
                @endif
            </td>
            <td>
                {{(DB::table('etudiants')->where('user_id',$reservation->etudiant_id)->get())[0]->etudiant_nom}}  {{(DB::table('etudiants')->where('user_id',$reservation->etudiant_id)->get())[0]->etudiant_prenom}}</td>
            <td>
                {{(DB::table('etudiants')->where('user_id',$reservation->etudiant_id)->get())[0]->etudiant_email}}
            </td>
            <td>
                @if($reservation->tuteur_id != null)
                
                    {{(DB::table('etudiants')->where('user_id',(DB::table('tuteur')->where('id',$reservation->tuteur_id)->get())[0]->user_id)->get())[0]->etudiant_email}}
                
                @endif
                @if($reservation->professeur_id != null)
                   {{DB::table('professeurs')->where('professeur_id',$reservation->professeur_id)->get()[0]->professeur_email}}
                @endif
            </td>
        </tbody>
        @endforeach
</table>
@endsection