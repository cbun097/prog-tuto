@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
<div class="col">
    <img class="rounded responsive w-75 p-3" src="{{ asset('images/profile-icon.png')}}">
</div>
    <div class="col">
        <div class="row">   
            <div class="col">
                {!! Form::label('nomProf', 'Nom:') !!} 
                <input type="text" value="{{$prof->professeur_nom}}" class="form-control" readonly/>
            </div>
            <div class="col">
                {!! Form::label('prenomProf', 'Prenom:') !!} 
                <input type="text" value="{{$prof->professeur_prenom}}" class="form-control" readonly/>
            </div>  
        </div>
        <div class="row">          
            <div class="col">
                <!--TODO: mettre un popup pour envoyer un email-->
                {!! Form::label('emailProf', 'Email:') !!} 
                <input type="text" value="{{$prof->professeur_email}}" class="form-control" readonly/>
            </div>
            <div class="col">
                {!! Form::label('telProf', 'Téléphone:') !!} 
                <input type="text" value="{{$prof->professeur_tel}}" class="form-control" readonly/>
            </div>  
        </div>
        <div class="form-group">
            {!! Form::label('departementProf', 'Département:') !!} 
            <input type="text" value="{{$prof->professeur_departement}}" class="form-control" readonly/>
        </div>

        <div class="form-group">
            {!! Form::label('coursProf', 'Cours:') !!} 
            <input type="text" value="{{$prof->professeur_cours}}" class="form-control" readonly/>
        </div>

        <div class="row">
            <div class="col">
                {!! Form::label('campusProf', 'Campus:') !!}  
                <input type="text" value="{{$prof->professeur_campus}}" class="form-control" readonly/>
            </div>
            <div class="col">
                {!! Form::label('localProf', 'Local:') !!} 
                <input type="text" value="{{$prof->professeur_local}}" class="form-control" readonly/>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('dispoProf', 'Disponibilités:') !!} 
            <input type="text" value="{{$prof->professeur_disponibilites}}" class="form-control" readonly/>
        </div>
        <button class="btn"><a href="/professeurs">Retour</a></button>
        <!--TODO: verifier si le user est connecter ou pas-->
        @if(Auth::check())
            <a href="{!! route('debutProf', ['professeur_id'=>$prof->professeur_id]) !!}" class="btn btn-dark">Demander de l'aide (réserver)</a> 
        @else
            <p style="color:red">Vous devez etre connecte pour reserver du temps avec cette personne.</p>
        @endif
</div>
</div>
</div>
<hr>
@endsection