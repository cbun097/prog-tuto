@extends('layouts.app')

@section('content')
<main role="main">
<h2>Les professeurs</h2>
<div class="container">
    <form action="index" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <div class="row">
            <div class="col-md-8">
                <input type="text" placeholder="Recherche d'un professeur" name="recherce" class="form-control"/>
            </div>
            <div class="col-md-4">
                <!--changer le logo pour une loupe-->
                <input type="submit" name="btnRecherche" value="Recherche" class="btn btn-primary btn-lg"/>
            </div>
        </div>
    </form>
</div>
<!--si l'utilisateur entre une recherche affiché le layout
pour les resultats-->

<!--sinon elle affiche tous les resultats possible-->
<div class="container">
<table class="table table-hover">
        <thead>
            <tr>
                <th>Nom du professeur</th>
                <th>Cours</th>
                <th>Département</th>
                <th>Local</th>
                <th>Campus</th>
            </tr>
        </thead>
        @foreach ($professeurs as $prof)
        <tbody>
            <!--TODO:changer pour le professeur-->
                <tr>
                    <!--prenom, nom-->
                <td><a href="/professeurs/{{$prof->professeur_id}}">{{$prof->professeur_prenom}} {{$prof->professeur_nom}}</a></td>
                    <!--change selon ce que l'utilisateur entre dans 
                    la barre de recherche. Une fois le professeur
                    recherché est trouvé, celui-ci peut cliquer sur le nom
                    pour voir les informations du professeur et ses disponibilites
                    afin de prendre un rendezvous-->
                    <td>{{$prof->professeur_cours}}</td>
                    <td>{{$prof->professeur_departement}}</td>
                    <td>{{$prof->professeur_local}}</td>
                    <td>{{$prof->professeur_campus}}</td>
                </tr>    
        </tbody>
        @endforeach
    </table>   
    </div>
    <hr/>
</main>
@endsection

