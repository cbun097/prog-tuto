@extends('layouts.app')

@section('content')
<main role="main">
    <?php
        $reservation = $rsv;
        $tuteur = DB::table('tuteur')->where('id',$reservation->tuteur_id)->get();
        $professeur = DB::table('professeurs')->where('professeur_id',$reservation->professeur_id)->get();
        if($rsv->tuteur_id != null)   
        {
            $etudiantTuteur = DB::table('etudiants')->where('etudiant_id',$tuteur[0]->etudiant_id)->get(); 
        }        
    ?>
    <div class="container" style="padding:20px;border:2px solid black;">     
        <div class="text-center">  
        <h2 style="color:green;">La réservation a été sauvegarder!</h2>
        @if($reservation->tuteur_id != null)
            <p>Vous avez pris rendez-vous avec <u> {{$etudiantTuteur[0]->etudiant_nom}} {{$etudiantTuteur[0]->etudiant_prenom}}</u> le <b>{{$reservation->reservation_date}}</b> pour le cours de <b>{{$reservation->cours_reservation}}</b>.</p>
            <p>Commentaire/Questions: {{$reservation->reservation_commentaire_questions}}</p>  
            <p>Communiquer: <b> {{$etudiantTuteur[0]->etudiant_email}}</b> pour tout changement.</p>   
        @endif
         @if($reservation->professeur_id != null)
            <p>Vous avez pris rendez-vous avec <u>{{$professeur[0]->professeur_nom}} {{$professeur[0]->professeur_prenom}} </u> le <b>{{$reservation->reservation_date}}</b> pour le cours de <b>{{$reservation->cours_reservation}}</b>.</p>
            <p>Commentaire/Questions: {{$reservation->reservation_commentaire_questions}}</p>  
            <p>Communiquer: <b> {{$professeur[0]->professeur_email}}</b> pour tout changement.</p>   
         @endif   
         <a class="btn btn-secondary" href="{{route('evenements',['id'=> Auth::User()->user_id])}}">Date à retenir</a>
    </div>
    </div>
</main>
@endsection