@extends('layouts.app')

@section('content')
<main role="main">
<!--Source: https://getbootstrap.com/docs/4.0/examples/jumbotron/-->
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron"  style="background: url(images/banner.jpg) no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
          <div class="container">
            <h2 class="display-3" style="color:white;font-weight:bold;">Programme de tutorat </h2>
            <h3 style="color:white;">au Cégep de l'Outaouais</h3>
          </div>
        </div>
  
        <div class="container">
          <!-- Example row of columns -->
          <div class="row">
            <div class="col-md-4">
              <h2>Témoignages</h2>
              <p>Nous avons reçu beaucoup de commentaires positifs sur ce programme qui a permis la réussite des étudiants du Cégep de l'Outaouais.
              <br>
              Et, nous espérons que vous aimerez votre expérience avec ce programme et que vous trouvez l'aide que vous avez besoin. 
              </p>              
            </div>
            <div class="col-md-4">
              <h2>But du programme</h2>
              <p>Nous avons crée ce le programme de tutorat au Cégep de l'Outaouais afin de facilité  les étudiants de tous les programmes a retrouvé de l'aide.<br>
              Après une réservation, il est de votre responsabitlité de vous présenter à l'heure au local demandé.</p>
            </div>
            <div class="col-md-4">
              <h2>Contact</h2>
              <p>Si vous rencontrez des problèmes techniques ou des suggestions.
              <br> Veuillez consulter cette page pour contacter les administrateurs de ce programme</p>
              <p><a href="/contact" class="btn btn-secondary" href="#" role="button">Contacts  &raquo;</a></p>
            </div>
          </div>
  
          <hr>
  
        </div> <!-- /container -->
  </main>
@endsection