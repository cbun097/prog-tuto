<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('app.name','Prog-tuto')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/app.css')}}" />
    <script src="main.js"></script>
</head>

<body>
    <!--source: https://getbootstrap.com/docs/4.1/examples/sign-in/-->
    <div class="text-center">
        <img src="{{ asset('images/logo.png')}}">
    </div>
    <form method="POST" action="/login">
        {{ csrf_field() }}
    <div class="container">
        <form class="form-signin">
            <h1 class="h3 mb-3 font-weight-normal">Connexion</h1>       
            <input type="number" name="user_id" class="form-control" placeholder="Numéro Étudiant" required autofocus>            
            <input type="password" class="form-control" placeholder="Password" name="password" required>
            <div class="form-check mb-3">
                <input type="checkbox" value="remember-me" class="form-check-input" id="souvenirIdentifiant">
                <label class="form-check-label" for="souvenirIdentifiant"> Se souvenir de moi</label>
            </div>
            <div class="row">
                <div class="col-sm-6"> 
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
                </div>
                <div class="col-sm-6">
                    <a href="{{ route('register') }}" class="btn btn-lg btn-success btn-block">S'enregistrer</a>
                </div>
            </div>
            <p class="mt-5 mb-3 text-muted">&copy; Claire Bun & Jacob Johnston -  2018</p>
        </form>
    </div>
    </form>
</body>
</html>