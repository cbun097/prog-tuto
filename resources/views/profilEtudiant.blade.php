@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col">
        <img class="rounded responsive w-75 p-3" src="{{ asset('images/profile-icon.png')}}">
        <input type="file" class="form-control-file" name="imageFile" id="imageFile" aria-describedby="fileHelp">
    </div>
    <div class="col">
@foreach ($etudiants as $data)
@if(Auth::user()->user_id == $data->user_id)
<form method="POST" action={{ url('/ModifierEtudiant/' . $data->etudiant_id ) }}>
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" name="etudiant_id" value="{{$data->etudiant_id}}" class="form-control"/>
    <div class="row">     
        <div class="col">
            <div class="form-group">
                {!! Form::label('etudiant_nom', 'Nom:') !!} 
                {{Form::text('title', $data->etudiant_nom, ['class'=>'form-control', 'id'=>'name', 'name'=>'etudiant_nom'])}}
            <p></p>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                {!! Form::label('etudiant_prenom', 'Prenom:') !!}
                {{Form::text('title', $data->etudiant_prenom, ['class'=>'form-control', 'id'=>'name', 'name'=>'etudiant_prenom'])}}
            </div>
        </div>      
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                {!! Form::label('emailEtudiant', 'Email:') !!}
                {{Form::text('title', $data->etudiant_email, ['class'=>'form-control', 'id'=>'name', 'name'=>'etudiant_email'])}}
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                {!! Form::label('etudiant_tel', 'Téléphone:') !!}
                {{Form::text('title', $data->etudiant_tel, ['class'=>'form-control', 'id'=>'name', 'name'=>'etudiant_tel'])}}
            </div>
        </div>      
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                {!! Form::label('etudiant_programme', 'Programme:') !!}  
                {{Form::text('title', $data->etudiant_programme, ['class'=>'form-control', 'id'=>'name', 'name'=>'etudiant_programme'])}}          
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                {!! Form::label('etudiant_campus', 'Campus:') !!}
                {{Form::text('title', $data->etudiant_campus, ['class'=>'form-control', 'id'=>'name', 'name'=>'etudiant_campus'])}}
            </div>
        </div>      
    </div>
    <div class="form-group">
        {!! Form::label('etudiant_bio', 'A propos de moi:') !!}
        {{Form::text('title', $data->etudiant_bio, ['class'=>'form-control', 'id'=>'name', 'name'=>'etudiant_bio'])}}
    </div>
    @if($data->estTuteur == 'true')    
    <?php
        $tuteur = DB::table('tuteur')->where('etudiant_id',$data->etudiant_id)->get()[0]
    ?>
        <div class="form-group">
            {!! Form::label('CoursDonnée', 'Cours donné:') !!}
            <input type="text" value="{{$tuteur->tuteur_cours}}" class="form-control">
        </div>
        <div class="form-group">
            {!! Form::label('disposEtudiant', 'Disponibilités tuteur:') !!}
            <textarea type="text" class="form-control">{{$tuteur->disponibiliteTuteur}}</textarea>
        </div>
    @endif
    {{Form::hidden('_method', 'PUT')}}
    <button style="cursor:pointer" type="submit" class="btn btn-success">Modifier Profil</button>
</form>
    <button class="btn btn-info" name="btnDemandeTuteur" id="btnDemandeTuteur" data-toggle="modal" data-target="#modalTuteur">Demande Tuteur</button>
    <button class="btn btn-danger" name="btnDeleteEtudiant" data-toggle="modal" data-target="#modalDelete">Supprimer mon compte</button>
    </div>
</div>


    <!--le modal pour une demande de tuteur etudiant-->      
<div class="modal fade" id="modalTuteur" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<form method="Post">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <!-- Modal header -->
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Je veux devenir tuteur(e)</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <!-- Modal body -->
            {!!Form::open(['action'=>['ProfilController@updateEstTuteur', $data->user_id],'method'=>'PUT'])!!}
            <div class="modal-body">
                <p>Conditions:</p>
                <div class="form-group form-check">
                    {{Form::checkbox('estTuteur','true',['class'=>'form-check-input'])}}
                    <label class="form-check-label" for="exampleCheck1">Je desire devenir tuteur</label>
                    <input type="hidden" value="{{$data->user_id}}" name="user_id"/>
                    <input type="hidden" value="{{$data->etudiant_id}}" name="etudiant_id"/>
                    <div class="form-group">
                        {!! Form::label('CoursDonnée', 'Cours donné:') !!}
                        <input type="text" name="coursTuteur" value="" class="form-control">
                    </div>
                    <div class="form-group">
                        {!! Form::label('disposEtudiant', 'Disponibilités tuteur:') !!}
                        <textarea type="text" name="disposEtudiant" value="" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                {{Form::submit('Confirmer',['class'=>'btn btn-primary'])}}
            </div>
            {!!Form::close()!!}
        </div>
    </div>
    </form>
</div>

<!--le modal pour supprimer un etudiant-->      
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <!-- Modal header -->
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation de la suppression de votre compte, {{$data->etudiant_prenom}} {{$data->etudiant_nom}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" style="display:inline;">
                <p>Etes-vous sûr de vouloir supprimer votre compte?</p>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            {!!Form::open(['action'=>['UserController@destroy', $data->user_id],'method'=>'POST'])!!}
            {{Form::hidden('_method','DELETE')}}
            {{Form::submit('Confirmer',['class'=>'btn btn-primary'])}}
            {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
@endif
@endforeach
@endsection