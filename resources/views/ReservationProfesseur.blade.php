@extends('layouts.app')

@section('content')
    {!! Form::open(['route'=>'addReservation'])!!}
    <div class="text-center">
        <input hidden name="professeur" value="{{$professeurs[0]->professeur_id}}"/> 
        <h3>Réservation: {{$professeurs[0]->professeur_prenom}} {{$professeurs[0]->professeur_nom}}</h3>
    </div>
    
    <div class="form-group">
        {{Form::label('datesDispos','Dates disponibles:',['class' => 'control-label'])}}
        <input type="text" class="form-control" name="date" value=" {{$professeurs[0]->professeur_disponibilites}}" readonly/>
    </div>
    <div class="form-group">
            {{Form::label('journeeReserve','Date de réservation:',['class' => 'control-label'])}}
            {!! Form::date('date', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Date']); !!} 
        </div>
    <div class="form-group">
        {{Form::label('nomCours','Cours:',['class' => 'control-label'])}}
        <select class="form-control" name="nomCoursSelec">
            <option value="null">Sélectionner le cours</option>
            <option value="{{$professeurs[0]->professeur_cours}}">{{$professeurs[0]->professeur_cours}}</option>
        </select>
    </div>
    <div class="form-group">
        {{Form::label('commentaire','Commentaires/Questions:',['class' => 'control-label'])}}
        {{Form::textarea('commentaireQuestion',null,['class' => 'form-control'])}}
    </div>
        <button class="btn"><a href="/professeurs">Retour</a></button>
        {{Form::submit('Confirmer',['class' => 'btn'])}}
    {!! Form::close() !!}

@endsection
