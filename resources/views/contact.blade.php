@extends('layouts.app')

@section('content')
<main role="main">
<div class="container">
<div class="row">
<div class="col-md">
    <h1>Les créateurs</h1>
    <div class="row">
    <div class="card w-50">
  <div class="card-body">
    <h5 class="card-title">Claire Bun</h5>
    <p class="card-text">Étudiante de 3ieme année en technique de l'informatique. Graduation mai 2019.</p>
  </div>
</div>
<div class="card w-50">
  <div class="card-body">
    <h5 class="card-title">Jacob Johnston</h5>
    <p class="card-text">Étudiant de 3ieme année en technique de l'informatique. Graduation mai 2019.</p>
  </div>
</div>
    </div>
</div>
<div class="col-md">
    <div class="text-center">
        <img src="{{ asset('images/logo.png')}}">
    </div>
    <form>
        <div class="form-group">
            <h1>Contactez-nous</h1>
            <div class="row">
                <div class="col-md-6">
                    <input type="text" placeholder="Nom" name="nom" class="form-control"/>
                </div>
                <div class="col-md-6">
                    <input type="text" placeholder="Prénom" name="prenom" class="form-control"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input typw="text" placeholder="Adresse Électronique" name="email" class="form-control"/>
        </div>
        <div class="form-group">
            <textarea class="form-control" placeholder="Votre message" id="message" name="message" rows="3"></textarea>
        </div>
        <input type="submit" name="btnEnvoyer" value="Envoyer" class="btn btn-primary btn-lg"/>
    <form>
</div>
</div>
</div>
<hr/>
@endsection