@extends('layouts.app')

@section('content')
<main role="main">
<h2>Les tuteurs</h2>
<div class="container">
        <form action="/searchTuto" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-8">
                    <input type="text" placeholder="Recherche d'un étudiant tuteur" name="rechercheTuteur" class="form-control"/>
                </div>
                <div class="col-md-4">
                    <!--changer le logo pour une loupe-->
                    <button type="submit" name="btnRecherche" value="Recherche" class="btn btn-primary btn-lg">Recherche</button>
                </div>
            </div>
        </form>
    </div>  
<!--si l'utilisateur entre une recherche affiché le layout
pour les resultats-->

<!--sinon elle affiche tous les resultats possible-->
<div class="container">
   <table class="table table-hover">
        <thead>
            <tr>
                <th>Nom de l'étudiant tuteur</th>
                <th>Programme</th>
                <th>Cours</th>
            </tr>
        </thead>
          @foreach($tuteur as $tuto)
        <tbody>
            <td>
                <?php
                    $etudiant = DB::table('etudiants')->where('etudiant_id',$tuto->etudiant_id)->get()[0]
                ?>
                <a href="/tuteurs/{{$tuto->id}}">{{$etudiant->etudiant_prenom}} {{$etudiant->etudiant_nom}}</a>
            </td>
            <td>{{$etudiant->etudiant_programme}}</td>

            <td>{{$tuto->tuteur_cours}}</td>           
        </tbody>
        @endforeach
    </table>   
</div>
<hr/>
</main>
@endsection