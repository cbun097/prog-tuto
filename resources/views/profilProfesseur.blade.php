@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col">
        <img class="rounded responsive w-75 p-3" src="{{ asset('images/profile-icon.png')}}">
        <input type="file" class="form-control-file" name="imageFile" id="imageFile" aria-describedby="fileHelp">
    </div>
    <div class="col">
@foreach ($professeurs as $prof)
    @if(Auth::user()->user_id == $prof->user_id)
    <form method="POST" action={{ url('/ModifierProf/' . $prof->professeur_id ) }}>
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <input type="hidden" name="id" value="{{$prof->professeur_id}}" class="form-control"/>
    <div class="row">
       
        <div class="col">
            <div class="form-group">
                {!! Form::label('nomProf', 'Nom:') !!} 
                {{Form::text('title', $prof->professeur_nom, ['class'=>'form-control', 'id'=>'name', 'name'=>'professeur_nom'])}}
            <p></p>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                {!! Form::label('prenomProf', 'Prenom:') !!}
                {{Form::text('title', $prof->professeur_prenom, ['class'=>'form-control', 'id'=>'name', 'name'=>'professeur_prenom'])}}
            </div>
        </div>      
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                {!! Form::label('emailProf', 'Email:') !!}
                {{Form::text('title', $prof->professeur_email, ['class'=>'form-control', 'id'=>'name', 'name'=>'professeur_email'])}}          
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                {!! Form::label('telProf', 'Téléphone:') !!}
                {{Form::text('title', $prof->professeur_tel, ['class'=>'form-control', 'id'=>'name', 'name'=>'professeur_tel'])}}
            </div>
        </div>      
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                {!! Form::label('departementProf', 'Département:') !!}  
                {{Form::text('title', $prof->professeur_departement, ['class'=>'form-control', 'id'=>'name', 'name'=>'professeur_departement'])}}        
            </div>
        </div>
        <div class="col">
        <div class="form-group">
                {!! Form::label('coursProf', 'Cours:') !!}
                {{Form::text('title', $prof->professeur_cours, ['class'=>'form-control', 'id'=>'name', 'name'=>'professeur_cours'])}}
            </div>
        </div>      
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                {!! Form::label('campusProf', 'Campus:') !!}
                {{Form::text('title', $prof->professeur_campus, ['class'=>'form-control', 'id'=>'name', 'name'=>'professeur_campus'])}}
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                {!! Form::label('localProf', 'Local:') !!}
                {{Form::text('title', $prof->professeur_local, ['class'=>'form-control', 'id'=>'name', 'name'=>'professeur_local'])}}
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('dispoProf', 'Disponibilités:') !!} 
        {{Form::text('title', $prof->professeur_disponibilites, ['class'=>'form-control', 'id'=>'name', 'name'=>'professeur_dispo'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
<button style="cursor:pointer" type="submit" class="btn btn-success">Modifier profil</button>
</form>

<button class="btn btn-danger" name="btnDeleteEtudiant" data-toggle="modal" data-target="#modalDeleteProf">Supprimer mon compte</button> 
    </div>
</div>
 

<!--le modal pour supprimer un etudiant-->      
<div class="modal fade" id="modalDeleteProf" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <!-- Modal header -->
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation de la suppression de votre compte, {{$prof->professeur_prenom}} {{$prof->professeur_nom}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
            <p>Etes-vous sûr de vouloir supprimer votre compte?</p>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            {!!Form::open(['action'=>['UserController@destroy', $prof->user_id],'method'=>'POST'])!!}
            {{Form::hidden('_method','DELETE')}}
            {{Form::submit('Confirmer',['class'=>'btn btn-primary'])}}
            {!!Form::close()!!}
        </div>
        </div>
    </div>
</div>
@endif
@endforeach
@endsection
