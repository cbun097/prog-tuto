<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('app.name','Prog-tuto')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/app.css')}}" />
    <script src="main.js"></script>
</head>
<body>

<div class="container">
        <div class="text-center">
            <img src="{{ asset('images/logo.png')}}">
        </div>

    <h2 class="text-center">Créer un compte</h2>
    <form method="POST" action="/register">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Je suis:</label>
            <select class="form-control" name="user_status">
                <option name="Étudiant" value="etudiant">Étudiant<option>
                <option name="Professeur" value="professeur">Professeur<option>
            </select>
        </div>
        <div class="form-group">
            <label for="name">Numéro d'identification:</label>
            <input type="number" class="form-control" id="name" name="user_id">
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Nom:</label>
                    <input type="text" class="form-control" id="name" name="user_name">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="name">Prénom:</label>
                    <input type="text" class="form-control" id="name" name="user_prenom">
                </div>
            </div>
        </div>
       
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="user_email">
        </div>

        <div class="form-group">
            <label for="programme">Programme:</label>
            <select class="form-control" name="user_programme">
                <option name="null" value="null">Choisir votre programme...<option>
                <option name="informatique" value="Technique Informatique">Technique Informatique de gestion<option>
                <option name="sciencesNat" value="Sciences de la nature">Sciences de la nature<option>
                <option name="sciencesHumaines" value="Sciences Humaines">Sciences Humaines<option>
            </select>
        </div>

        <div class="form-group">
            <label for="campus">Campus:</label>
            <select class="form-control" name="user_campus">
                <option name="Gab-Roy" value="Gabrielle-Roy">Gabrielle-Roy<option>
                <option name="Felix-Leclerc" value="Felix-Leclerc">Felix-Leclerc<option>
                <option name="Louis-Reboul" value="Louis-Reboul">Louis-Reboul<option>
            </select>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="password_confirmation">Password Confirmation:</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                </div>
            </div>
        </div>
        <div class="form-group text-center">
             <!--etre login directement-->
             <button style="cursor:pointer" type="submit" class="btn btn-success">Enregistrer</button>
            <!--retourne a la page de login en case d'annuler-->
                <button style="cursor:pointer" type="submit" class="btn btn-danger">Annuler</button>            
        </div>
        @include('include.messages')
    </form>
</div>
</body>
</html>