<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('app.name','Prog-tuto')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/app.css')}}" /> 
    <link rel="stylesheet" href="{{asset('css/style.css')}}"/> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>  
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

</head>
<body>
    <!--inclure la page pour la navigation-->
    @include('include.navbar')
    <div class="container">
        <!--inclure la page pour les messages d'erreurs-->
        @include('include.messages')
        <!--afficher le contenu de la pages avec le layout-->
        @yield('content')
        <!--ajouter la page JS-->
        @yield('jsfile')

    </div>
</body> 
<footer class="container text-center">
    <p>&copy; Claire Bun & Jacob Johnston -  2018</p>
    <p>Cégep de l'Outaouais</p>
</footer>
</html> 