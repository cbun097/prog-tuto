<!--source: https://getbootstrap.com/docs/4.1/examples/pricing/-->
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal"><a href="/">{{config('app.name','Prog-tuto')}}</a></h5>
        <nav class="navbar navbar-expand-lg navbar-light  my-2 my-md-0 mr-md-3">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="p-2 text-dark" href="/tuteurs">Tuteurs</a>
                </li>
                <li class="nav-item">
                    <a class="p-2 text-dark" href="/professeurs">Professeurs</a>
                </li>
                <li class="nav-item">
                    <a class="p-2 text-dark" href="/outils">Outils</a>
                </li>
                <li class="nav-item">
                    <a class="p-2 text-dark" href="/contact">Contact</a>
                </li>
            </ul>
           {{-- Right Side Of Navbar --}}
           <ul class="navbar-nav ml-auto">
                {{-- Authentication Links --}}
                @guest
                    <a class="btn btn-outline-primary" href="/login">Connexion</a>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>                       
                            {{ Auth::user()->user_prenom }} <span class="caret"></span>
                        </a>
                        <!--fonctionne pas...-->
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item {{ Request::is('profil/'.Auth::user()->user_id, 'profil/'.Auth::user()->user_id . '/edit') ? 'active' : null }}" href="{{ url('/profil/'.Auth::user()->user_id) }}">
                               Profil
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('evenements',['id'=> Auth::User()->user_id])}}">Évènements à venir</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}">Déconnexion</a>
                        </div>
                    </li>
                @endguest              
            </ul>
        </nav>
</div>


  