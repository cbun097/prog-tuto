@extends('layouts.app')

@section('content')
    {!! Form::open(['route'=>'addReservation'])!!}
    <?php
        $etudiant = DB::table('etudiants')->where('etudiant_id', $tuteur[0]->etudiant_id)->get();        
    ?>
    <div class="text-center">
        <input hidden name="tuteur" value="{{$tuteur[0]->id}}"/>   
        <h3>Réservation: {{$etudiant[0]->etudiant_prenom}} {{$etudiant[0]->etudiant_nom}}  </h3>
    </div>
   
    <div class="form-group">
        {{Form::label('datesDispos','Dates disponibles:',['class' => 'control-label'])}}
        <input class="form-control" type="text" value="{{$tuteur[0]->disponibiliteTuteur}}" name="dateR" readonly/>
    </div>
    <div class="form-group">
        {{Form::label('journeeReserve','Date de réservation:',['class' => 'control-label'])}}
        {!! Form::date('reservation_date', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Date']); !!} 
    </div>

    <div class="form-group">
        {{Form::label('nomCours','Cours:',['class' => 'control-label'])}}
        <select class="form-control" name="cour_reservation">
            <option value="null">Sélectionner le cours</option>
        <option value="{{$tuteur[0]->tuteur_cours}}">{{$tuteur[0]->tuteur_cours}}</option>
        </select>
    </div>
    <div class="form-group">
        {{Form::label('commentaire','Commentaires/Questions:',['class' => 'control-label'])}}
        {{Form::textarea('commentaireQuestion',null,['class' => 'form-control'])}}
    </div>
        <button class="btn"><a href="/tuteurs">Retour</a></button>
        {{Form::submit('Confirmer',['class' => 'btn'])}}
    {!! Form::close() !!}
@endsection
