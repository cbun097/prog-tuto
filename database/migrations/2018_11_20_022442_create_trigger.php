<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *ss
     * @return void
     */
    //ce trigger sert a insert les users dans leur propre
    public function up()
    {
        DB::unprepared("CREATE TRIGGER `tr_insert_users_tables` 
            AFTER INSERT ON `users` 
            FOR EACH ROW 
            IF NEW.user_status = 'etudiant' 
            THEN
                INSERT INTO etudiants(user_id, etudiant_nom, etudiant_prenom, etudiant_email, etudiant_programme, etudiant_campus, created_at, updated_at)
                    VALUES(NEW.user_id, NEW.user_name, NEW.user_prenom, NEW.user_email, NEW.user_programme, NEW.user_campus, NEW.created_at, NEW.updated_at);
            ELSE       
                Insert INTO professeurs(professeur_nom, professeur_prenom, professeur_email, professeur_campus, user_id, created_at, updated_at)
                    VALUES (NEW.user_name, NEW.user_prenom, NEW.user_email, NEW.user_campus, NEW.user_id, NEW.created_at, NEW.updated_at);
            END IF;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `tr_insert_users_tables`');
    }
}
