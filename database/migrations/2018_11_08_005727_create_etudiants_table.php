<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtudiantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etudiants', function (Blueprint $table) {
            $table->increments('etudiant_id',1000);
            $table->string('user_id');
            $table->string('etudiant_nom');
            $table->string('etudiant_prenom');
            $table->string('etudiant_email');
            $table->string('etudiant_tel')->nullable();
            $table->string('etudiant_programme');
            $table->string('etudiant_campus');
            $table->string('estTuteur')->nullable();
            $table->mediumText('etudiant_bio')->nullable();
            $table->string('etudiant_img')->nullable();
            $table->timestamps();
        });

        Schema::table('etudiants', function($table){
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etudiants');
    }
}
