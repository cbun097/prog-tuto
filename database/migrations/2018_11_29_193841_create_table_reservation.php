<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReservation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation', function (Blueprint $table) {
            $table->increments('id',1000);
            $table->date('reservation_date');
            $table->integer('etudiant_id')->unsigned();
            $table->integer('professeur_id')->unsigned()->nullable();            
            $table->integer('tuteur_id')->unsigned()->nullable();      
            $table->string('cours_reservation');
            $table->text('reservation_commentaire_questions')->nullable();              
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation');     
    }
}
