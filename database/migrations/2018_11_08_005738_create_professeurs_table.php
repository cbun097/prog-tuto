<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesseursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professeurs', function (Blueprint $table) {
            $table->increments('professeur_id',10);
            $table->string('professeur_nom');
            $table->string('professeur_prenom');
            $table->string('professeur_email');
            $table->string('professeur_tel')->nullable();
            $table->string('professeur_campus');
            $table->string('professeur_departement')->nullable();
            $table->string('professeur_cours')->nullable();
            $table->string('professeur_local')->nullable();
            $table->string('professeur_disponibilites')->nullable();
            $table->string('user_id');           
            $table->timestamps();
        });

        Schema::table('professeurs', function ($table){
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professeurs');
    }
}
