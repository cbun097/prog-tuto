<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtudiantTuteurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tuteur', function (Blueprint $table) {
            $table->increments('id',100);
            $table->integer('etudiant_id')->unsigned();          
            $table->string('user_id');         
            $table->text('disponibiliteTuteur')->nullable();
            $table->string('tuteur_cours')->nullable();
            $table->timestamps();
        });

        Schema::table('tuteur', function($table){
            $table->foreign('etudiant_id')->references('etudiant_id')->on('etudiants')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tuteur');
    }
}
