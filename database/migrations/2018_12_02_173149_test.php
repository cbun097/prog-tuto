<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Test extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("CREATE TRIGGER `update_user_from_prof` AFTER UPDATE ON `professeurs`
        FOR EACH ROW UPDATE users 
                   SET user_name = NEW.professeur_nom, user_prenom = NEW.professeur_prenom, user_email = NEW.professeur_email, 
                   user_campus = NEW.professeur_campus, created_at = NEW.created_at, updated_at = NEW.updated_at
                   WHERE user_id = NEW.user_id
    ");

DB::unprepared("CREATE TRIGGER `update_user_from_etud` AFTER UPDATE ON `etudiants`
FOR EACH ROW UPDATE users 
           SET user_name = NEW.etudiant_nom, user_prenom = NEW.etudiant_prenom, user_email = NEW.etudiant_email, user_programme = NEW.etudiant_programme,
           user_campus = NEW.etudiant_campus, created_at = NEW.created_at, updated_at = NEW.updated_at
           WHERE user_id = NEW.user_id
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
