<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professeurs extends Model
{
    //infos table professeurs
    protected $table = 'professeurs';  
    protected $primaryKey = 'professeur_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'professeur_id',
        'user_id',
        'professeur_nom',
        'etudiant_prenom',
        'professeur_prenom',
        'professeur_email',
        'professeur_tel',
        'professeur_campus',
        'professeur_departement',
        'professeur_cours',
        'professeur_local',
        'professeur_disponibilites'
    ];

    //relation avec la table users
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
