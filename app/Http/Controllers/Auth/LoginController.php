<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //fonction pour get la vue du login
    public function getLogin()
    {
        return view('login');
    }

    //fonction pour le post du login
    public function postLogin(Request $request)
    {
        $credentials = $request->only('user_id','password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...    
            return redirect()->to('/');
        }
        else{
            //message d'erreur pas ete capable se de connecter  
        
                 abort (403,'Connexion erreur. Veuillez vous assurer que votre compte est crée ou avoir bien entré votre mot de passe');
            
        }
    }

    //methode pour se deconnecter
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
