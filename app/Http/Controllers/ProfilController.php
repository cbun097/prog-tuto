<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Etudiants;
use Illuminate\Support\Facades\Auth;
use App\Professeurs;
use App\Tuteur;

class ProfilController extends Controller
{
    /**
     * Prendre la liste du user selon un prof ou etudiant
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $etudiants = Etudiants::All();
        $professeurs = Professeurs::All();
        //si c'est un etudiant 
        if(Auth::user()->user_status == 'etudiant')
        {
            //retourne la vue profil etudiant
           return view('profilEtudiant',['etudiants' => $etudiants]);
        } 
        
        //si c'est un professeur
        if(Auth::user()->user_status == 'professeur')
        {
            //retourne la vue du profil professeur
            return view('profilProfesseur',['professeurs' => $professeurs]);
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //$prof = Professeurs::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    //update la table etudiant s'il veut devenir tuteur et insert dans la table tuteur
    public function updateEstTuteur(Request $request, $etudiant_id)
    {
        //pour le modal, insert si le l'etudiant est un tuteur
        $estTuteur = $request->input('estTuteur');
        $etudiant = $request->input('user_id');

        if($estTuteur == 'true')
        {
            //insert dans la table etudiant dans le champs estTuteur
            Etudiants::where(['user_id'=> $etudiant])->update(['estTuteur'=> $estTuteur]);
            //insert les informations dans la table  Tuteur
            Tuteur::create([
                'etudiant_id'=> $request->input('etudiant_id'),
                'user_id' => $etudiant,
                'disponibiliteTuteur'=>$request->input('disposEtudiant'),
                'tuteur_cours'=>$request->input('coursTuteur')
            ]);
            
            return redirect('profil/{users}');
        }
        else{
            Etudiants::where(['user_id'=> $etudiant])->update(['estTuteur'=> 'false']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
