<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Etudiants;
use App\Professeurs;
use App\User;
use DB;

class RegisterController extends Controller
{
    //source: https://vegibit.com/how-to-create-user-registration-in-laravel/
    public function create()
    {
        return view('register');
    }

    //inserer un nouvel utilisateur
    public function insert(Request $resquest)
    {
        //la validation des donnees
        $this->validate(request(), [
            'user_status' => 'required',
            'user_id' => 'required',
            'user_name' => 'required',
            'user_prenom' => 'required',
            'user_email' => 'required|email',
            'user_programme' => 'required',
            'user_campus'=>'required',
            'password' => 'required|confirmed',
        ]);
        
        //insert dans la table users
        $user = User::create(request(['user_status','user_id','user_name', 'user_prenom','user_email','user_programme','user_campus', 'password']));
        
        auth()->login($user);

        //trigger: inser dans la table soit etudiant ou professeur
        //selon le status entre

        return redirect()->to('/');
    }
}
