<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Professeurs;
use App\Etudiants;
use DB;
use App\User;
use App\Tuteur;
use App\Reservation;
use Carbon\Carbon;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reserverTuteur($id)
    {
        $tuteur=DB::table('tuteur')->where('etudiant_id',$id)->get();
        
        //retourne la vue profil etudiant
        return view('ReservationTuteur',['tuteur' => $tuteur]);
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reserverProfeseur($professeur_id)
    {
        $professeurs=Professeurs::all();
        //insert dans l'evement dans la table reservation
        $professeurs = Professeurs::where('professeur_id',$professeur_id)->get();
        //retourne la vue du profil professeur
        return view('ReservationProfesseur',['professeurs' => $professeurs]);
                 
    }

    /**
     * Insert dans la table reservation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //la validation des donnees pour faire une reservation
        $this->validate(request(), [
            'reservation_date' => 'required|after:today',
            'cour_reservation' => 'required',
        ]);


        //la sauvegarde dans la table
        $rsv = new Reservation();
        $rsv->reservation_date=$request->input('reservation_date');
        
        $rsv->professeur_id=$request->input('professeur');
        $rsv->etudiant_id = Auth::User()->user_id;
        $rsv->tuteur_id=$request->input('tuteur');
        $rsv->cours_reservation=$request->input('cour_reservation');
        $rsv->reservation_commentaire_questions=$request->input('commentaireQuestion');
        $rsv->save();

        //retourne la vue du profil professeur
        return view('confirmation',['rsv' => $rsv]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //prendre la liste des events selon le id de l'etudiant
    public function getEvents($etudiant_id)
    {
        $rsv = Reservation::where('etudiant_id', Auth::User()->user_id);
        return view('events',['rsv'=>$rsv]);
    }

}
