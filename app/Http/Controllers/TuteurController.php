<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tuteur;
use DB;

class TuteurController extends Controller
{
    //afficher la liste des tuteurs dans la vue de la liste de tuteur
    public function index()
    {
        $tuteur = DB::table('tuteur')->get();
        return view('tuto')->with('tuteur', $tuteur);
    }

    //afficher le profil du tuteur selon celui qui a ete selectionner
    public function show($id)
    {
        $etudiant_tuteur = Tuteur::find($id);
        return view('postTuto')->with('etudiant_tuteur',$etudiant_tuteur);
    }
}
