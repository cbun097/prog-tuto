<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Professeurs;
use Illuminate\Support\Facades\Auth;

class ProfesseursController extends Controller
{
    /**
     * methode qui prend la liste des professeurs dans la BD
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professeurs = Professeurs::all();
        return view('professeurs')->with('professeurs',$professeurs);
    }

    //prendre les informations selon le id du professeur
    public function show($id)
    {
        $prof = Professeurs::find($id);
        return view('postProfesseur')->with('prof', $prof);
    }

    /**
     * Update le professeur selon son id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $professeur_id)
    {
        //la validation des donnees
        $this->validate(request(), [
            'professeur_nom' => 'required',
            'professeur_prenom' => 'required',
            'professeur_email' => 'required|email',
            'professeur_tel' => 'required',
            'professeur_departement'=>'required',
            'professeur_cours'=>'required',
            'professeur_campus'=>'required',
            'professeur_local'=>'required'
        ]);
        
        //update dans la table professeurs
        $etudiant = Professeurs::find($professeur_id);
        $etudiant->professeur_nom = $request->input('professeur_nom');
        $etudiant->professeur_prenom = $request->input('professeur_prenom');
        $etudiant->professeur_email = $request->input('professeur_email');
        $etudiant->professeur_tel = $request->input('professeur_tel');
        $etudiant->professeur_departement = $request->input('professeur_departement');
        $etudiant->professeur_cours = $request->input('professeur_cours');
        $etudiant->professeur_campus = $request->input('professeur_campus');
        $etudiant->professeur_local = $request->input('professeur_local');
        $etudiant->professeur_disponibilites = $request->input('professeur_dispo');
        $etudiant->save();
        //trigger: inser dans la table soit etudiant ou professeur
        //selon le status entre
        //retourner vers la page de profil
        return redirect()->route('profil',[$etudiant->user_id]);
    }
}
