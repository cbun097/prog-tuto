<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Etudiants;
use DB;

class EtudiantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //methode qui prend tous la liste des étudiants dans la BD
    public function index()
    {
        $etudiants = Etudiants::all();
        return view('etudiants')->with('etudiants', $etudiants);
    }

    /**
     * Update l'etudiant selon son id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $etudiant_id)
    {
        //la validation des donnees
        $this->validate(request(), [
            'etudiant_nom' => 'required',
            'etudiant_prenom' => 'required',
            'etudiant_email' => 'required|email',
            'etudiant_programme' => 'required',
            'etudiant_campus'=>'required',
            'etudiant_tel'=>'required'
        ]);
        //update dans la table etudiant 
        $etudiant = Etudiants::find($etudiant_id);
        $etudiant->etudiant_nom = $request->input('etudiant_nom');
        $etudiant->etudiant_prenom = $request->input('etudiant_prenom');
        $etudiant->etudiant_email = $request->input('etudiant_email');
        $etudiant->etudiant_programme = $request->input('etudiant_programme');
        $etudiant->etudiant_campus = $request->input('etudiant_campus');
        $etudiant->etudiant_tel = $request->input('etudiant_tel');
        $etudiant->etudiant_bio = $request->input('etudiant_bio');
        $etudiant->save();
        //trigger: inser dans la table soit etudiant ou professeur
        //selon le status entre
        //retourner vers la page de profil de l'étudiant
        return redirect()->route('profil',[$etudiant->user_id]);
    }
}
