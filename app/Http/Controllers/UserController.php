<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //delete l'utilisateur de la table etudiant et professeur
    public function destroy($user_id)
    {
        $user= User::find($user_id);     
        $user->delete();    
        return view('index');
    }
}
