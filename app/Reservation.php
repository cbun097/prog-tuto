<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    //infos table reversations
    protected $table = 'reservation';  
    protected $primarykey = 'id';

    protected $fillable = [
        'id',
        'reservation_date',
        'etudiant_id',
        'professeur_id',
        'tuteur_id',
        'cours_reservation',
        'reservation_commentaire_questions',
    ];
}
