<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    //infos table users
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_status',
        'user_id',
        'user_name',
        'user_prenom',
        'user_email',
        'user_programme',
        'user_campus', 
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //ajouter pour le password soit hashed
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    //relation avec la table Etudiants
    public function etudiants()
    {
        return $this->hasMany('App\Etudiants','user_id');
    }

    //relation avec la table professeurs
    public function professeur()
    {
        return $this->hasMany('App\Professeurs','user_id');
    }

    protected static function boot() {
        parent::boot();
    
        static::deleting(function($user_id) {        
            $user_id->professeur()->delete();               
        });

        static::deleting(function($user_id) {
            $user_id->etudiants()->delete();
        });
    }
}
