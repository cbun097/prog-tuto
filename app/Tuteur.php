<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tuteur extends Model
{
    //infos table tuteurs
    protected $table = 'tuteur';  
    protected $primarykey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'etudiant_id',
        'user_id',
        'disponibiliteTuteur',
        'tuteur_cours',
    ];

    //relation  a la table etudiants
    public function etudiants()
    {
        return $this->belongsTo('App\Etudiants','etudiant_id');
    }
}
