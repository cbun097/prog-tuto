<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etudiants extends Model
{
    //infos table etudiants
    protected $table = 'etudiants';  
    protected $primaryKey = 'etudiant_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'etudiant_id',
        'user_id',
        'etudiant_nom',
        'etudiant_prenom',
        'etudiant_email',
        'etudiant_tel',
        'etudiant_programme',
        'etudiant_campus',
        'estTuteur',
        'etudiant_bio',
    ];

    //relation avec la table users
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //relation a la table tuteur
    public function tuteur()
    {
        return $this->hasMany('App\Tuteur');
    }
}
